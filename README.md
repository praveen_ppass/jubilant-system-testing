This repository contains the code of a POC seeking to run a React application including React,Axios,Reactstrap/Bootstrap.

### Installation

* clone the repository
* install the dependencies with `npm install`

### Run on local machine

You can follow the steps.

*  running command `npm start`

### Task completion Time 

* I have complete this task within 1 hours.

### To create a production build, use `npm run build`

@In case of any query conatct   -  praveen.ppass@gmail.com
