import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Multiselect } from 'multiselect-react-dropdown';

const Search = (props) => {
    const { optionData } = props;
    return (
        <Container className="text-center">
            <Row>
                <Col md="12" sm="12">
                    <h3>Search Movies</h3>
                    <Multiselect
                        options={optionData}
                        selectionLimit="5"
                        placeholder="search movie"
                        caseSensitiveSearch="true"
                        displayValue="Title" // Property Title to display in the dropdown options
                    />

                </Col>
            </Row>
        </Container>
    );
};

export default Search;