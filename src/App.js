import React, { Component } from 'react';
import './App.css';
import Search from "./components/search";
import axios from 'axios';
class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      optionData: []
    }
  }
  componentWillUnmount() {
    document.removeEventListener('input', this.activeEvent);
  }

  activeEvent = () => {
    var search_input = document.getElementById("search_input");
    search_input.addEventListener("input", () => {
      if (search_input.value !== "" && search_input.value) {
        axios.get(`http://omdbapi.com/?s=${search_input.value}&apikey=6a4c0e5f&plot=short&page=1`)
          .then(res => {
            const { data, status } = res;
            if (status === 200) {
              const { Search } = data;
              this.setState({ optionData: Search });
            }
          }).catch(err => {
            console.log(err)
          })
      }
    });
  }
  componentDidMount() {
    if (typeof window !== "undefined") {
      this.activeEvent()
    }
  }
  render() {
    const { optionData } = this.state;
    return (
      <div className="App">
        <Search optionData={optionData} />
      </div>
    );
  }
}

export default App;
